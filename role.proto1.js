

module.exports = {
    // a function to run the logic for this role
    run: function(creep) {
        var tgt1 = Game.getObjectById(creep.memory.tgt1);
        var errange = Game.getObjectById(creep.memory.errange);
        var sourceid = Game.getObjectById(creep.memory.sourceid);
        // if creep is bringing energy to the spawn but has no energy left
        if (creep.memory.working == true && creep.carry.energy == 0) {
            // switch state
            creep.memory.working = false;
        }
        // if creep is harvesting energy but is full
        else if (creep.memory.working == false && creep.carry.energy == creep.carryCapacity) {
            // switch state
            creep.memory.working = true;
        }

        // if creep is supposed to transfer energy to the spawn
        if (creep.memory.working == true) {
            // try to transfer energy, if the spawn is not in range
            if (creep.transfer(Game.spawns.Spawn1, tgt1) == errange) {
                // move towards the spawn
                creep.moveTo(Game.spawns.Spawn1);
            }
        }
        // if creep is supposed to harvest energy from source
        else {
            // find closest source
            var source = creep.pos.findClosestByPath(FIND_SOURCES);
            // try to harvest energy, if the source is not in range
            if (creep.harvest(source) == errange) {
                // move towards the source
                creep.moveTo(source);
            }
        }
    }
};