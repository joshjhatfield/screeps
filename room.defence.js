/**
 * Created by Josh on 17/07/2016.
 */
module.exports = {
    run: function (mainroom) {

        var towers = Game.rooms[mainroom].find(FIND_STRUCTURES, {
            filter: (s) => s.structureType == STRUCTURE_TOWER
        });

        for (let tower of towers) {
            var target = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
            if (target != undefined) {
                tower.attack(target);
            }
        }
    }
};