/**
 * Created by Josh on 5/07/2016.
 */
//this is a fixed repair script, it is not as complex as the original by th_pion.
var roleBuilder = require('role.builder');

module.exports = {
    // a function to run the logic for this role
    run: function(creep) {
        // if creep is trying to repair something but has no energy left
        if (creep.memory.working == true && creep.carry.energy == 0) {
            // switch state
            creep.memory.working = false;
        }
        // if creep is harvesting energy but is full
        else if (creep.memory.working == false && creep.carry.energy == creep.carryCapacity) {
            // switch state
            creep.memory.working = true;
        }

        // if creep is supposed to repair something
        if (creep.memory.working == true) {
            // find closest structure with less than max hits
            // Exclude walls because they have way too many max hits and would keep
            // our repairers busy forever. We have to find a solution for that later.
            var structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {

                // Use for different levels of development.
                // Use this one for a mature base.
                //filter: (s) => s.hits < s.hitsMax && s.structureType == STRUCTURE_WALL
                //use this one when you start, set the s.hits value low
                filter: (s) => s.hits <= 100000 && s.structureType == STRUCTURE_WALL
            });

            // if we find one
            if (structure != undefined && creep.memory.buildtarget == 'none') {
                // try to repair it, if it is out of range
                //creep.memory.buildtarget = structure;
                if (creep.repair(structure) == ERR_NOT_IN_RANGE) {
                    // move towards it
                   creep.moveTo(structure);
                }
            }
            // if we can't fine one
            else {
                // look for construction sites
                roleBuilder.run(creep);
            }
        }
        // if creep is supposed to harvest energy from source
        else {
            // find closest source
            var source = creep.pos.findClosestByPath(FIND_SOURCES);
            // try to harvest energy, if the source is not in range
            if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                // move towards the source
                creep.moveTo(source);
            }
        }
    }
};