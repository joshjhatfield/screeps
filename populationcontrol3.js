/**
 * Created by Josh on 15/07/2016.
 */
module.exports = {
    run: function (creep) {
        //population limits
        var minimumharvesters = 4;
        var minimumupgraders = 8;
        var minimumbuilders = 1;
        var minimumrepairers = 4;
        var minimumproto1 = 1;

        //Ratios
        var numberofharvesters = _.sum(Game.creeps, (c) => c.memory.role == 'harvester');
        var numberofupgraders = _.sum(Game.creeps, (c) => c.memory.role == 'upgrader');
        var numberofbuilders = _.sum(Game.creeps, (c) => c.memory.role == 'builder');
        var numberofrepairers = _.sum(Game.creeps, (c) => c.memory.role == 'repairer');
        var numberofproto1 = _.sum(Game.creeps, (c) => c.memory.role == 'proto1');



        //minersss
        if (numberofharvesters < minimumharvesters) {
            Game.spawns.Spawn1.createCreep([WORK, CARRY, MOVE, MOVE], undefined,
                {role: 'harvester', working: false, caste: 'gcl2'});
        }

        //upgraders
        if (numberofupgraders < minimumupgraders) {
            Game.spawns.Spawn1.createCreep([WORK, WORK, CARRY, MOVE], undefined,
                {role: 'upgrader', working: false, caste: 'gcl2'});
        }
        //builders
        if (numberofbuilders < minimumbuilders) {
            Game.spawns.Spawn1.createCreep([WORK, CARRY, MOVE, MOVE], undefined,
                {role: 'builder', working: false, caste: 'gcl2'});
        }
        if (numberofrepairers < minimumrepairers) {
            Game.spawns.Spawn1.createCreep([WORK, CARRY, MOVE, MOVE], undefined,
                {role: 'repairer', working: false, caste: 'gcl2'});
        }
        if (numberofproto1 < minimumproto1) {
            Game.spawns.Spawn1.createCreep([WORK, WORK, CARRY, MOVE], undefined,
                {role: 'proto1', working: false, caste: 'gcl2', tgt1: 'RESOURCE_ENERGY', errange: 'ERR_NOT_IN_RANGE'});
        }

    }
};

