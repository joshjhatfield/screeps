
// Main run script
//put room ID here When starting fresh
var mainroom = 'E27N42';

//operational scripts
var populationcontrol = require('population.control');
var populationcontrol2 = require('populationcontrol2');
var populationcontrol3 = require('populationcontrol3');


//Creep AI
var roleharvester = require('role.harvester');
var roleupgrader = require('role.upgrader');
var rolebuilder = require('role.builder');
var rolerepairer = require('role.repairer');
var rolewallrep = require('role.wallrepairer');

//base defence
var basedefence = require('room.defence');

//main logic
module.exports.loop = function () {
    //Clear Memory
    for (let name in Memory.creeps) {
        if (Game.creeps[name] == undefined) {
            delete Memory.creeps[name];
        }
    }

    // for every creep name in Game.creeps
    for (let name in Game.creeps) {
        // get the creep object
        var creep = Game.creeps[name];

        //direct creeps to their work.
        if (creep.memory.role == 'harvester') {
            roleharvester.run(creep);
        }
        else if (creep.memory.role == 'upgrader') {
            roleupgrader.run(creep);
        }
        else if (creep.memory.role == 'builder') {
            rolebuilder.run(creep);
        }
        else if (creep.memory.role == 'repairer') {
            rolerepairer.run(creep);
        }
        else if (creep.memory.role == 'wallrepair') {
            rolewallrep.run(creep);
       }
    }
    //Population growth
    if (Game.rooms[mainroom].controller.level <= '2' && Game.rooms[mainroom].energyAvailable >= '300') {
        populationcontrol.run(creep);
    }
        // This works on the assumption that you have atleast 5x extensions and a tower, change the energy level otherwise.
    else if (Game.rooms[mainroom].controller.level > '2' && Game.rooms[mainroom].energyAvailable > '300') {
        populationcontrol2.run(creep);
   }

    // Defend from enemies
    basedefence.run(mainroom)
};

